from django.shortcuts import render

nama = "Firdhan Hilmy Purnomo"
jurusan = "Ilmu Komputer"
npm = "1706026992"

def index(request):
    author = "Firdhan Hilmy Purnomo"
    response = {'nama':nama, 'jurusan':jurusan, 'npm':npm, 'author': author}
    return render(request, 'AppProfile/profile.html', response)