from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

class HomePageStatus(TestCase):
    def test_profile_url_is_exist(self):
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200) 
    
    def test_status_page_returns_correct_html(self):
        response = self.client.get('/profile/')  
        self.assertTemplateUsed(response, 'AppProfile/profile.html') 