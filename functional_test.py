from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest


class Story6FunctTest(unittest.TestCase):  

    def setUp(self):  
        self.browser  = webdriver.Chrome('./chromedriver')

    def tearDown(self):  
        self.browser.quit()
    

    def test_status_input(self):  
        # Edith has heard about a cool new online status app. She goes
        # to check out its homepage
        self.browser.get('https://story6.herokuapp.com/')

        #She notices that the title of the website is "Firdhan Purnomo"
        self.assertIn('Firdhan Purnomo', self.browser.title)
        #She knows that the header says "Kow's your feeling?" 
        header_text = self.browser.find_element_by_tag_name('h3').text
        self.assertIn("How's your feeling?", header_text)
        #She 
        navbar = self.browser.find_element_by_css_selector('#navbar_header')
        self.assertEqual(
            navbar.value_of_css_property('background-color'),
            'rgba(0, 0, 255, 1)'
        )
        navbar_text = self.browser.find_element_by_css_selector('.navbar-brand')
        self.assertIn(
            'Arial',
            navbar_text.value_of_css_property('font-family')
        )
        self.assertEqual(
            navbar_text.value_of_css_property('font-size'),
            '30px'
        )

        # She is invited to enter a to-do item straight away
        self.browser.implicitly_wait(5)
        inputbox = self.browser.find_element_by_id('item_text')
        self.assertEqual(
            inputbox.get_attribute('placeholder'),
            'What are you feeling right now?'
        )

        submit = self.browser.find_element_by_id('submit')

        #She is feeling happy right now
        inputbox.send_keys('I am so happy')

        # When she hits enter, the page updates, and now the page lists
        # "I am so happy" as an item in a status table
        submit.send_keys(Keys.RETURN) 
        time.sleep(1)
        self.assertIn('I am so happy', self.browser.page_source)
        

if __name__ == '__main__':  
    unittest.main(warnings='ignore')  



