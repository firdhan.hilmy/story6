# Story 6

PPW Group Project

Nama:

	Firdhan Hilmy Purnomo (1706026992)

Link Herokuapp :
https://story6.herokuapp.com

Status Pipelines: 
[![pipeline status](https://gitlab.com/firdhan.hilmy/story6/badges/master/pipeline.svg)](https://gitlab.com/firdhan.hilmy/story6/commits/master)

Status Code Coverage:
[![coverage report](https://gitlab.com/firdhan.hilmy/story6/badges/master/coverage.svg)](https://gitlab.com/firdhan.hilmy/story6/commits/master)

