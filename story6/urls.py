"""story6 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views
from AppLandPage.views import status_page, logout_red
import AppProfile.urls as AppProfile
import AppFavBook.urls as AppFavBook
import AppSubscribe.urls as AppSubscribe
import AppFavBook.views as fav_book
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('logout/', logout_red, name="logout"),
    path('', status_page, name='home'),
    path('profile/', include(AppProfile)),
    path('fav-book/', include(AppFavBook), name="books_indexs"),
    path('get-books/', fav_book.get_books, name='get_books'),
    path('clicked-fav/', fav_book.clicked_fav, name='clicked_fav'),
    path('subscribe/', include(AppSubscribe), name="subscribe"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
