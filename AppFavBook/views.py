from django.shortcuts import render, redirect
import requests
import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import logout

def get_books(request):
        new_data_books = {}
        q = json.dumps(request.GET['q'])
        print(q)
        books_data = requests.get(
            "https://www.googleapis.com/books/v1/volumes?q=" + q).json()["items"]
        for item in books_data:
            new_data_books[item["id"]] = {"title": item["volumeInfo"]["title"],
                                          "authors": item["volumeInfo"]["authors"] if "authors" in item["volumeInfo"] else "undefined",
                                          "publishedDate": item["volumeInfo"]["publishedDate"] if "publishedDate" in item["volumeInfo"] else "undefined",
                                          "publisher": item["volumeInfo"]["publisher"] if "publisher" in item["volumeInfo"] else "undefined",
                                          "description": item["volumeInfo"]["description"] if "description" in item["volumeInfo"] else "undefined",
                                          "image": item["volumeInfo"]["imageLinks"]["thumbnail"] if "imageLinks" in item["volumeInfo"] and "thumbnail" in item["volumeInfo"]["imageLinks"] else "undefined",
                                          "pageCount": item["volumeInfo"]["pageCount"] if "pageCount" in item["volumeInfo"] else "undefined",
                                          "is_fav":False}
        
        if request.user.is_authenticated:
            books_list = request.session[request.user.email]['book_id']
            for key in new_data_books.keys():
                if key in books_list:
                    new_data_books[key]['is_fav'] = True
        return JsonResponse({'data': new_data_books})


def books_index(request):
    if request.user.is_authenticated:
        print("auth")
        request.session[request.user.email] = {
            'fav_count': request.session.get(request.user.email, {}).get('fav_count', 0), "book_id": request.session.get(request.user.email, {}).get('book_id', [])}
        request.session.modified = True
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
        return render(request, 'AppFavBook/favbook.html', {'author': "Firdhan Hilmy Purnomo", 'fav_count': request.session[request.user.email]['fav_count']})
    else:
        fav_count = 0
        print("gomen")
        return render(request, 'AppFavBook/favbook.html', {'author': "Firdhan Hilmy Purnomo", 'fav_count': fav_count})


@csrf_exempt
def clicked_fav(request):
    if request.user.is_authenticated:
        print("hooray")
        book = request.POST['book']
        books_list = request.session[request.user.email]['book_id']
        fav_count = request.session[request.user.email]['fav_count']
        if book in books_list:
            is_fav = False
            fav_count -= 1
            request.session[request.user.email]['fav_count'] = fav_count
            books_list.remove(book)
            request.session[request.user.email]['book_id'] = books_list
            request.session.modified = True
            return JsonResponse({'is_fav': is_fav, 'fav_count': fav_count, 'is_auth': True})
        else:
            is_fav = True
            fav_count += 1
            request.session[request.user.email]['fav_count'] = fav_count
            request.session[request.user.email]['book_id'].append(book)
            request.session.modified = True
            return JsonResponse({'is_fav': is_fav, 'fav_count': fav_count, 'is_auth': True})

    else:
            return JsonResponse({'is_auth': False})

