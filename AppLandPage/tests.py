from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

from AppLandPage.views import status_page  
from AppLandPage.models import Item
from AppLandPage.forms import Status_Form


class StatusPageTest(TestCase):
    def test_status_url_is_exist(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_status_page_returns_correct_html(self):
        response = self.client.get('/')  
        self.assertTemplateUsed(response, 'AppLandPage/status.html')  

    def test_can_save_a_POST_request(self):
        # Creating a new status
        new_status = Item.objects.create(text='I am happy')
    
        # Retrieving all available activity
        counting_all_available_status = Item.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_status_input_has_placeholder(self):
        form = Status_Form()
        self.assertIn('class="todo-form-textarea', form.as_p())
        self.assertIn('id="item_text"', form.as_p())

    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            ["This field is required."]
        )

    def test_redirects_after_POST(self):
        response = self.client.post('/', data={'text': 'A new list item'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
    
    #def test_post_error_and_render_the_result(self):
     #   test = "Anon"
      #  response_post = self.client.post('/', data={'text': ''})
       # self.assertEqual(response_post.status_code, 302)

#        response = self.client.get('/')
 #       html_response = response.content.decode('utf8')
  #      self.assertNotIn(test, html_response)

    def test_only_saves_items_when_necessary(self):
        self.client.get('/')
        self.assertEqual(Item.objects.count(), 0)

    def test_displays_all_list_items(self):
        Item.objects.create(text='itemey 1')
        Item.objects.create(text='itemey 2')

        response = self.client.get('/')

        self.assertIn('itemey 1', response.content.decode())
        self.assertIn('itemey 2', response.content.decode())



class ItemModelTest(TestCase):
    
    def test_saving_and_retrieving_items(self):
        first_item = Item()
        first_item.text = 'The first (ever) list item'
        first_item.save()

        second_item = Item()
        second_item.text = 'Item the second'
        second_item.save()

        saved_items = Item.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.text, 'The first (ever) list item')
        self.assertEqual(second_saved_item.text, 'Item the second')
