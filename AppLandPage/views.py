from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from AppLandPage.models import Item
from AppLandPage.forms import Status_Form
from django.contrib.auth import logout

response = {}
def status_page(request):
    author = "Firdhan Hilmy Purnomo"
    form = Status_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['text'] = request.POST['text']
        items = Item(text=response['text'])
        items.save()
        return redirect('/')
    items = Item.objects.all()
    return render(request, 'AppLandPage/status.html', {'items': items, 'status_form': form, 'author': author})


def logout_red(request):
    logout(request)
    return redirect(status_page)
    

    
 
