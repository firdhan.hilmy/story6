from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi status',
    }
    text_attrs = {
        'type': 'text',
        'cols': 50,
        'rows': 4,
        'id': 'item_text',
        'class': 'todo-form-textarea',
        'placeholder':'What are you feeling right now?'
    }
    text = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=text_attrs))
