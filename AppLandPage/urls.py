from django.urls import path, include
from AppLandPage import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('', views.status_page),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
