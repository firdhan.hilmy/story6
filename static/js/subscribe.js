var isEmailValidAll = false;
var timer = null;
$('#input-email').keydown(function () {
    processForm();
    $("#emailDiv small").html("");
    clearTimeout(timer);
    timer = setTimeout(checkEmail, 500)
    processForm()
});

$("#input-name").on("input", function () {
    $("#emailDiv small").html("");
    processForm();
    clearTimeout(timer);
    timer = setTimeout(checkEmail, 500)
});

$("#input-password").on("input", function () {
    $("#emailDiv small").html("");
    processForm();
    clearTimeout(timer);
    timer = setTimeout(checkEmail, 500)
});

function checkEmail() {
    $("#emailDiv small").html("");

    data = {
        "input": $("#input-email").val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }

    $.ajax({
        type: "POST",
        url: "validate-email/",
        data: data,
        dataType: "json"
    })
    .done(isEmailAvailabe)
    .fail(function (res) {
         console.log(res)
    });
}

function isEmailAvailabe(data) {
    $("#emailDiv small").html("");
    console.log(data)
    if (data['status_code'] === 400) {
        $("#emailDiv").append(`<small class='error' style="color:red;">${data['message']}</small>`);
        isEmailValidAll = false;
        processForm();
    } else {
        console.log(data)
        $("#emailDiv").append(`<small class='success' style="color:green;">${data['message']}</small>`);
        isEmailValidAll = true;
        processForm();
    }
}

function processForm() {
    let isEmailValid = ($("#input-email").val() !== "") ? true : false;
    let isPasswordValid = ($("#input-password").val() !== "") ? true : false;
    let isNameValid = ($("#input-name").val() !== "") ? true : false;
    let isPassLengthValid = ($("#input-password").val().length > 5) ? true : false;

    if ($("#input-email").val() !== "" && $("#input-password").val() !== "" &&
        $("#input-name").val() !== "" && $("#input-password").val().length > 5 && isEmailValidAll) {
        $('#submitButton').prop('disabled', false);
    } else {
        $('#submitButton').prop('disabled', true);
    }
}


$("#submitButton").click(function(){
    data = {
        "name": $("#input-name").val(),
        "email": $("#input-email").val(),
        "password": $("#input-password").val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: "data/",
        data: data,
        dataType: "json",
    })

        .done(function (data) {
            if (data.status_code === 400) {
                alert(data.message);
            } else {
                alert(data.message);
            }
        })
        .fail(function (res) {
            alert(res)
        })
})