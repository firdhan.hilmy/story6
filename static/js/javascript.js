
var myVar;
function loadingFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
    document.getElementById("loader").style.display = "none";
    document.getElementById("wrapperDiv").style.display = "block";
}

$(document).ready(function () {
    $("#themeChg").click(function () {
        var bg = ["yellow", "maroon", "orange", "grey", "green", "aqua", "azure", "gold"];
        var randBg = bg[Math.floor(Math.random() * bg.length)];
        $("body").css("background", randBg);
    });
});