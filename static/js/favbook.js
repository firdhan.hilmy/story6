var is_favorite = {};
var count = 0;
var book_id = {}
$(document).ready(function () {
    $(".search-btn").click(function(){
        const book_title = $("#search").val();
        console.log(book_title);
        $('#table-book').hide();
        $("#loading").show();
        $.ajax({
            type: 'json',
            method: 'GET',
            url: '/get-books/?q=' + book_title, 
            success: function (result) {
                const book = result.data;
                let table = '';
                let i = 0;
                $.each(book, function (key, value) {
                    is_favorite[i] = value.is_fav;
                    console.log(is_favorite[i])
                    book_id[i] = key
                    table += `
        <tr class="table-primary">
            <td> <img src="${value.image}"/> <br> <br> ${value.title} </td>
            <td>${value.authors}</td>
            <td> ${value.publisher}, <br> ${value.publishedDate}</td>
            <td>${value.pageCount}</td>
            <td>${value.description}</td>
            <td><center>
              <img 
                style="filter: grayscale(${value.is_fav ? '0%' : '100%'}); cursor: pointer; width: 30px; height: 30px;" 
                src="${static_path}images/goldStar.png"
                id="fav_${i}"
                onclick="like(${i})"
              />
            </center></td>
        </tr>`;
                    i++;
                })
                $('#loading').hide();
                $('#table-book').show();
                $('#book_list').html(table);
                
            }

        });
    });
    
});

function like(i) {
    $.ajax({
        type: 'json',
        method: 'POST',
        url: '/clicked-fav/',
        data: { 'book': book_id[i]},
        success: function (result) {
            if(result.is_auth){
                const fav = result.is_fav
                const fav_count = result.fav_count
                if (fav) $("#fav_" + i).css('filter', 'grayscale(0%)');
                else $("#fav_" + i).css('filter', 'grayscale(100%)');
                $("#fav_count").html(fav_count);
            }
            else{
                if(is_favorite[i]){
                    is_favorite[i] = false;
                    count -= 1;
                    $("#fav_" + i).css('filter', 'grayscale(100%)');
                }
                else{
                    is_favorite[i] = true;
                    count += 1;
                    $("#fav_" + i).css('filter', 'grayscale(0%)');
                }
                $("#fav_count").html(count);
            }
            
            
        }
    })

}