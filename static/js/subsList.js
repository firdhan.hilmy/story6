var subsList;
$(document).ready(function(){
    $.ajax({
        type: 'json',
        method: 'GET',
        url: 'get-subs-list/',
        success: function (result){
            subsList = result;
            let table = '';
            $.each(result, function(index, value){
                table += `
        <tr class="table-primary" id="number_${value.id}">
            <td id="name_${value.id}">${value.name}</td>
            <td id="email_${value.id}">${value.email}</td>
            <td>
                <button id="subs_${value.id}" 
                    type="button" 
                    class="btn btn-danger"
                    onclick="unsubs(${value.id})">Unsuscribe</button>
            </td>
        </tr>`;
            })
            $("#subs-list").html(table);
        }
    });
});

function unsubs(i){
    data = {
        "email": subsList[i-1]['email'],
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: 'json',
        method: 'POST',
        url: 'unsubscribe/',
        data: data,
        dataType: 'json',
    })

    .done(function(data){
        $("#number_" + i).remove();
        alert(data.message)
    })

    .fail(function (res) {
        alert(res)
    })
}