from django.urls import path, include
from AppSubscribe import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.form_index),
    path('validate-email/', views.check_mail, name="check_mail"),
    path('data/', views.submit_subscriber, name="submit_subs"),
    path('subscriber-list/', views.listSubs_index, name="subs_list"),
    path('subscriber-list/get-subs-list/',
         views.get_subsList, name="get_subs_list"),
    path('subscriber-list/unsubscribe/',
         views.unsubscribe, name="unsubscribe"),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
