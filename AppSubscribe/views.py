from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core.validators import validate_email
from django.core.exceptions import ValidationError
from django.core import serializers
from django.db import IntegrityError

from .models import SubscribeForm

response = {}

def form_index(request):
    response['author'] = "Firdhan Hilmy Purnomo"
    return render(request, 'AppSubscribe/subscribe.html', response)

def listSubs_index(request):
    response['author'] = "Firdhan Hilmy Purnomo"
    return render(request, 'AppSubscribe/subsList.html', response)

def check_mail(request):

    try:
        emailInput = request.POST['input']
        validate_email(emailInput)
        print(emailInput)
    except:
        response['message'] = "E-mail is invalid"
        response['status_code'] = 400
        return JsonResponse(response)
    
    isSubscribe = SubscribeForm.objects.filter(email=emailInput)

    if(isSubscribe):
        response['message'] = "E-mail already exists. Please use another valid e-mail"
        response['status_code'] = 400
        return JsonResponse(response)
    else:
        response['message'] = "E-mail available"
        response['status_code'] = 200
        return JsonResponse(response, status=200)
    
def submit_subscriber(request):
    if(request.method == 'POST'):
        try:
            print("yeays masuk")
            emailSubs = request.POST['email']
            validate_email(emailSubs)
            subscribers = SubscribeForm.objects.create(name=request.POST['name'],
                                                       email=request.POST['email'],
                                                       password=request.POST['password'])
            subscribers = serializers.serialize('json', [subscribers, ])
            print(subscribers)

        except(ValidationError, IntegrityError) as error:
            response['message'] = "E-mail is invalid or already exists. Please use another valid e-mail"
            return JsonResponse(response, status=200)
        
        response['message'] = "Thank you for subscribing"
        response['subscribers'] = subscribers
        return JsonResponse(response, status=200)
    
    response['message'] = "GET method is not allowed"
    return JsonResponse(response, status=403)
        
def get_subsList(request):
    response['subscribers_list'] = list(SubscribeForm.objects.all().values())
    subs_list = response['subscribers_list']
    return JsonResponse(subs_list, safe=False)

def unsubscribe(request):
    unsubs_email = request.POST['email']
    SubscribeForm.objects.filter(email=unsubs_email).delete()
    response['message'] = "You Unsubscribed"
    return JsonResponse(response)

            
