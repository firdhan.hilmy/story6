from django import forms
from .models import SubscribeForm

class Form_Subscribe(forms.ModelForm):

    invalid_message = {
        'required': 'Please fill out this field',
        'invalid': 'Please fill out with valid input',
    }
    name_attrs = {
        'type': 'text',
        'class': 'subs-form-input',
        'placeholder': 'Please enter your full name',
    }
    email_attrs = {
        'type': 'email',
        'class': 'subs-form-input',
        'placeholder': 'Please enter your valid e-mail',
    }
    password_attrs = {
        'type': 'password',
        'class': 'subs-form-input',
        'placeholder': 'Please enter your password',
    }
    

    name = forms.CharField(
        label='Name:',
        max_length=255,
        required=True,
        widget=forms.TextInput(attrs=name_attrs)
    )


    email = forms.EmailField(
        label='E-Mail:',
        max_length=255,
        required=True,
        widget=forms.EmailInput(attrs=email_attrs)
    )

    password = forms.CharField(
        label='Password:',
        max_length=255,
        required=True,
        widget=forms.PasswordInput(attrs=password_attrs)
    )

    class Meta():
        model = SubscribeForm
        fields = ['name', 'email', 'password']

    
